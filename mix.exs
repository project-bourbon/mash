defmodule Bourbon.Mixfile do
  use Mix.Project

  def project do
    [
      app: :bourbon,
      version: "1.0.0",
      elixir: "~> 1.6",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix] ++ Mix.compilers(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ],
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Bourbon.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib", "priv/tasks"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.4.0-rc", override: true},
      {:phoenix_pubsub, "~> 1.0"},
      {:phoenix_ecto, "~> 3.2"},
      {:postgrex, ">= 0.0.0"},
      {:cowboy, "~> 2.3"},
      {:absinthe, "~> 1.4", override: true},
      {:absinthe_phoenix, "~> 1.4"},
      {:absinthe_ecto, "~> 0.1"},
      {:jason, "~> 1.0"},
      {:mix_test_watch, "~> 0.5", only: :dev, runtime: false},
      {:comeonin, "~> 4.1"},
      {:argon2_elixir, "~> 1.2"},
      {:faker, "~> 0.10", only: :test},
      {:distillery, "~> 1.5.2", runtime: false},
      {:hashids, "~> 2.0"},
      {:guardian, "~> 1.0.1"},
      {:timber, "~> 2.8.4"},
      {:ex_machina, "~> 2.2.0", only: :test},
      {:bodyguard, "~> 2.2.2"},
      {:excoveralls, "~> 0.10.0", only: :test},
      {:dataloader, "~> 1.0.4"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end
end
