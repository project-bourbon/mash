# Alias this container as the building container
FROM bitwalker/alpine-elixir-phoenix as builder

WORKDIR /mash

ARG SECRET_KEY_BASE
ARG SESSION_COOKIE_NAME
ARG SESSION_COOKIE_SIGNING_SALT
ARG SESSION_COOKING_ENCRYPTION_SALT
ARG DB_HOSTNAME
ARG DB_USERNAME
ARG DB_PASSWORD
ARG HASH_SECRET

ENV MIX_ENV=prod \
  SECRET_KEY_BASE=$SECRET_KEY_BASE \
  SESSION_COOKIE_NAME=$SESSION_COOKIE_NAME \
  SESSION_COOKIE_SIGNING_SALT=$SESSION_COOKIE_SIGNING_SALT \
  SESSION_COOKIE_ENCRYPTION_SALT=$SESSION_COOKIE_ENCRYPTION_SALT \
  DB_HOSTNAME=$DB_HOSTNAME \
  DB_USERNAME=$DB_USERNAME \
  DB_PASSWORD=$DB_PASSWORD \
  HASHID_SECRET=$HASHID_SECRET

ADD . .

RUN mix do deps.get, deps.compile

RUN cd deps/argon2_elixir && make clean && make

RUN mix release --env=prod --verbose

### Release

FROM alpine:3.6

RUN apk upgrade --no-cache && \
  apk add --no-cache bash openssl

ENV MIX_ENV=prod \
  SHELL=/bin/bash

WORKDIR /mash

COPY --from=builder /mash/_build/prod/rel/mash/releases/1.0.0/mash.tar.gz .

RUN tar zxf mash.tar.gz && rm mash.tar.gz

CMD ["/mash/bin/mash", "foreground"]
