defmodule Bourbon.Repo.Migrations.NullThreadFields do
  use Ecto.Migration

  def change do
    alter table("threads") do
      modify(:title, :string, null: false)
      modify(:privacy, :string, null: false)
    end
  end
end
