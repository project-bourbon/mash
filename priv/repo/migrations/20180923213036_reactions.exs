defmodule Bourbon.Repo.Migrations.Reactions do
  use Ecto.Migration

  def change do
    create table("reactions") do
      add(:user_id, references(:users))
      add(:thread_id, references(:threads))
      add(:type, :string, null: false)
      add(:identifier, :string, null: false)
    end

    create(unique_index(:reactions, [:type, :identifier]))
  end
end
