defmodule Bourbon.Repo.Migrations.CreateComments do
  use Ecto.Migration

  def change do
    create table("comments") do
      add(:user_id, references(:users))
      add(:thread_id, references(:threads))
      add(:body, :text)

      timestamps()
    end
  end
end
