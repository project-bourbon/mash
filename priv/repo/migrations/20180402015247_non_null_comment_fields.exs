defmodule Bourbon.Repo.Migrations.NonNullCommentFields do
  use Ecto.Migration

  def change do
    drop(constraint(:comments, "comments_user_id_fkey"))
    drop(constraint(:comments, "comments_thread_id_fkey"))

    alter table("comments") do
      modify(:user_id, references(:users), null: false)
      modify(:thread_id, references(:threads), null: false)
      modify(:body, :text, null: false)
    end
  end
end
