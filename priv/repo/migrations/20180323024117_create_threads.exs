defmodule Bourbon.Repo.Migrations.CreateThreads do
  use Ecto.Migration

  def change do
    create table("threads") do
      add(:user_id, references(:users))
      add(:title, :string)
      add(:text, :text)
      add(:link, :string)
      add(:privacy, :string)

      timestamps()
    end
  end
end
