defmodule Bourbon.Repo.Migrations.CreateAccounts do
  use Ecto.Migration

  def change do
    create table("users") do
      add(:username, :string, null: false)

      timestamps()
    end

    # Usernames are unique
    create(unique_index(:users, [:username]))

    create table("credentials") do
      add(:user_id, references(:users), null: false)
      add(:type, :string, null: false)
      add(:username, :string, null: false)
      add(:password_hash, :string, null: false)

      timestamps()
    end

    # Don't want duplicate credentials
    create(unique_index(:credentials, [:type, :username]))

    create table("profiles") do
      add(:user_id, references(:users), null: false)
      add(:name, :string)
      add(:bio, :text)
      add(:birthdate, :utc_datetime)
      add(:location, :string)

      timestamps()
    end
  end
end
