defmodule Bourbon.Repo.Migrations.ArchiveThreads do
  use Ecto.Migration

  def change do
    alter table("threads") do
      add(:archived_at, :utc_datetime)
    end
  end
end
