defmodule Bourbon.Repo.Migrations.AddTags do
  use Ecto.Migration

  def change do
    alter table("threads") do
      add(:tags, {:array, :string}, null: false)
    end
  end
end
