defmodule Bourbon.Posts.PostsTest do
  use Bourbon.DataCase, async: true
  alias Bourbon.{Accounts, Threads, Repo}
  alias Threads.{Thread, Reaction, Comment}

  describe "creating threads" do
    test "can create a thread" do
      user = insert(:user)

      assert {:ok,
              %Threads.Thread{
                title: "Test Title",
                link: "https://example.com",
                text: "Test Text",
                privacy: "public",
                tags: ["tagA", "tagB"]
              }} =
               Threads.create_thread(user, %{
                 title: "Test Title",
                 link: "https://example.com",
                 text: "Test Text",
                 privacy: :public,
                 tags: ["tagA", "tagB"]
               })
    end
  end

  describe "editing threads" do
    test "can edit your own thread" do
      user = insert(:user)
      thread = insert(:thread, user: user, title: "Old Title", privacy: "public", tags: ["tag-a"])

      assert {:ok, %Thread{title: "New Title"}} =
               Threads.edit_thread(thread, user, %{
                 title: "New Title"
               })
    end

    test "can't edit another user's thread" do
      user = insert(:user)
      thread_user = insert(:user)
      thread = insert(:thread, user: thread_user, title: "Old Title")

      assert {:error, _reason} =
               Threads.edit_thread(thread, user, %{
                 title: "New Title"
               })
    end

    test "can edit a thread if a moderator" do
      user = insert(:moderator_user)
      thread_user = insert(:user)
      thread = insert(:thread, user: thread_user, title: "Old Title")

      assert {:ok, %Thread{title: "New Title"}} =
               Threads.edit_thread(thread, user, %{
                 title: "New Title"
               })
    end

    test "can edit a thread if an admin" do
      user = insert(:admin_user)
      thread_user = insert(:user)
      thread = insert(:thread, user: thread_user, title: "Old Title")

      assert {:ok, %Thread{title: "New Title"}} =
               Threads.edit_thread(thread, user, %{
                 title: "New Title"
               })
    end
  end

  describe "getting threads" do
    test "can get threads" do
      insert_list(3, :thread)
      assert [%Thread{}, %Thread{}, %Thread{}] = Threads.get_threads(first: 10, from: 0)
    end

    test "can get a thread by its ID" do
      %{id: thread_id} = insert(:thread)
      assert %Thread{id: ^thread_id} = Threads.get_thread_by_id(thread_id)
    end
  end

  describe "reacting to threads" do
    test "can create a reaction" do
      user = insert(:user)
      thread = insert(:thread)

      assert {:ok, %Reaction{type: "emoji", identifier: "1f603"}} =
               Threads.react_to_thread(user, thread, %{type: "emoji", identifier: "1f603"})
    end

    test "can remove your reaction" do
      user = insert(:user)
      thread = insert(:thread)
      reaction = insert(:reaction, user: user, thread: thread)

      assert {:ok, %Reaction{}} = Threads.remove_reaction_from_thread(reaction, user)
    end

    test "can't remove someone else's reaction" do
      [user, reacting_user] = insert_pair(:user)
      thread = insert(:thread)
      reaction = insert(:reaction, user: reacting_user, thread: thread)

      assert {:error, _reason} = Threads.remove_reaction_from_thread(reaction, user)
    end
  end

  describe "deleting threads" do
    test "can delete your own thread" do
      user = insert(:user)
      thread = insert(:thread, user: user)

      assert {:ok, %Thread{}} = Threads.delete_thread(thread, user)
    end

    test "can't delete someone else's thread" do
      [user, thread_user] = insert_pair(:user)
      thread = insert(:thread, user: thread_user)

      assert {:error, _reason} = Threads.delete_thread(thread, user)
    end

    test "can delete a thread if a moderator" do
      user = insert(:moderator_user)
      thread_user = insert(:user)
      thread = insert(:thread, user: thread_user)

      assert {:ok, %Thread{}} = Threads.delete_thread(thread, user)
    end

    test "can delete a thread if an admin" do
      user = insert(:admin_user)
      thread_user = insert(:user)
      thread = insert(:thread, user: thread_user)

      assert {:ok, %Thread{}} = Threads.delete_thread(thread, user)
    end
  end

  describe "archiving threads" do
    test "can't archive a thread as a regular user" do
      user = insert(:user)
      thread = insert(:thread, user: user)

      assert {:error, _reason} = Threads.archive_thread(thread, user)
    end

    test "can archive a thread as a moderator" do
      user = insert(:moderator_user)
      thread_user = insert(:user)
      thread = insert(:thread, user: thread_user)

      {:ok, %Thread{archived_at: archived_at}} = Threads.archive_thread(thread, user)
      assert archived_at != nil
    end

    test "can archive a thread as an admin" do
      user = insert(:admin_user)
      thread_user = insert(:user)
      thread = insert(:thread, user: thread_user)

      {:ok, %Thread{archived_at: archived_at}} = Threads.archive_thread(thread, user)
      assert archived_at != nil
    end

    test "can't comment on a archived thread as a regular user" do
      user = insert(:user)
      thread = insert(:thread, %{archived_at: DateTime.utc_now()})

      assert {:error, _reason} = Threads.create_comment(user, thread, %{body: "Sup"})
    end

    test "can't react to a archived thread" do
      user = insert(:user)
      thread = insert(:thread, %{archived_at: DateTime.utc_now()})

      assert {:error, _reason} =
               Threads.react_to_thread(user, thread, %{type: "emoji", identifier: "smiley"})
    end
  end

  describe "getting comments" do
    test "can get a comment by its ID" do
      user = insert(:user)
      thread = insert(:thread, user: user)
      %Comment{id: comment_id} = insert(:comment, thread: thread, user: user)

      assert %Comment{id: comment_id} = Threads.get_comment_by_id(comment_id)
    end
  end

  describe "creating comments" do
    test "can comment on a thread" do
      creating_user = insert(:user)
      user = insert(:user, username: "CommentingUser")
      thread = insert(:thread, title: "Say hi if you're cool", user: creating_user)

      assert {:ok, %Threads.Comment{} = comment} =
               Threads.create_comment(user, thread, %{body: "hi"})

      assert %Threads.Comment{
               body: "hi",
               thread: %Threads.Thread{title: "Say hi if you're cool"},
               user: %Accounts.User{username: "CommentingUser"}
             } = comment |> Repo.preload([:user, :thread])
    end
  end

  describe "deleting comments" do
    test "can delete your own comment" do
      user = insert(:user)
      thread = insert(:thread, user: user)
      %Threads.Comment{id: comment_id} = comment = insert(:comment, user: user, thread: thread)

      assert {:ok, %Comment{id: ^comment_id}} = Threads.delete_comment(comment, user)
      refute Threads.get_comment_by_id(comment_id)
    end

    test "moderators can delete any comment" do
      user = insert(:moderator_user)
      creating_user = insert(:user)
      thread = insert(:thread, user: creating_user)
      %Comment{id: comment_id} = comment = insert(:comment, thread: thread, user: creating_user)

      assert {:ok, %Comment{id: ^comment_id}} = Threads.delete_comment(comment, user)
    end

    test "admins can delete any comment" do
      user = insert(:admin_user)
      creating_user = insert(:user)
      thread = insert(:thread, user: creating_user)
      %Comment{id: comment_id} = comment = insert(:comment, thread: thread, user: creating_user)

      assert {:ok, %Comment{id: ^comment_id}} = Threads.delete_comment(comment, user)
    end
  end

  describe "editing comments" do
    test "can edit your own comment" do
      user = insert(:user)
      thread = insert(:thread)
      comment = insert(:comment, thread: thread, user: user, body: "old text")

      assert {:ok, %Threads.Comment{body: "new text"}} =
               Threads.edit_comment(comment, user, %{
                 body: "new text"
               })
    end

    test "can't edit another user's comment" do
      user = insert(:user)
      comment_user = insert(:user)
      thread = insert(:thread)
      comment = insert(:comment, thread: thread, user: comment_user, body: "old text")

      assert {:error, _reason} =
               Threads.edit_comment(comment, user, %{
                 body: "new text"
               })
    end

    test "can edit another user's comment if admin" do
      user = insert(:admin_user)
      comment_user = insert(:user)
      thread = insert(:thread)
      comment = insert(:comment, thread: thread, user: comment_user, body: "old text")

      assert {:ok, %Threads.Comment{body: "new text"}} =
               Threads.edit_comment(comment, user, %{body: "new text"})
    end

    test "can edit another user's comment if a moderator" do
      user = insert(:moderator_user)
      comment_user = insert(:user)
      thread = insert(:thread)
      comment = insert(:comment, thread: thread, user: comment_user, body: "old text")

      assert {:ok, %Threads.Comment{body: "new text"}} =
               Threads.edit_comment(comment, user, %{body: "new text"})
    end
  end
end
