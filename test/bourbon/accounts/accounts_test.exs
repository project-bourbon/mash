defmodule Bourbon.Accounts.AccountsTest do
  use Bourbon.DataCase, async: true
  require Logger
  alias Bourbon.{TestHelpers, Repo, Accounts}
  alias Bourbon.Accounts.{User, Profile, Credential}

  test "can create a user" do
    assert {:ok, %User{}} =
             Accounts.create_user(%{
               user: %{username: "bobby"},
               credential: %{type: :email, username: "test@test.test", password: "password"},
               profile: %{birthdate: DateTime.utc_now()}
             })
  end

  test "can get a profile" do
    assert {:ok, user} =
             Accounts.create_user(%{
               user: %{username: "bobby"},
               credential: %{type: :email, username: "test@test.test", password: "password"},
               profile: %{name: "Bob Testerino"}
             })

    assert %User{profile: %Profile{name: "Bob Testerino"}} = user |> Repo.preload([:profile])
  end

  @tag :only
  test "can create a user without a profile" do
    assert %User{} = insert(:user, profile: %{})
  end

  test "can edit a user" do
    user = insert(:user)

    assert {:ok, %User{username: "test-user-2"}} =
             Accounts.edit_user(user, %{username: "test-user-2"})

    user = user |> Repo.preload([:profile])

    assert {:ok, %Profile{bio: "Howdy!", user_id: user_id}} =
             Accounts.edit_user_profile(user, %{bio: "Howdy!"})

    assert user_id == user.id

    user = user |> Repo.preload([:credentials])
    [user_credential] = user.credentials

    assert {:ok, %Credential{username: "test2@test.test"}} =
             Accounts.edit_credential(user_credential, %{username: "test2@test.test"})
  end

  @nonexistant_id 99_999_999_999_999
  test "can find a user" do
    user = insert(:user)

    assert {:ok, %User{id: found_id}} = Accounts.get_user_by_id(user.id)
    assert found_id == user.id
    assert {:error, _reason} = Accounts.get_user_by_id(@nonexistant_id)
  end

  test "users have a user role when created" do
    assert {:ok, %User{role: "user"}} =
             Accounts.create_user(%{
               user: %{username: "regular-joe"},
               credential: %{type: :email, username: "user@user.user", password: "userpass"}
             })
  end

  test "can create an admin user" do
    assert {:ok, %User{role: "admin"}} =
             Accounts.create_admin_user(%{
               user: %{username: "AdminBro"},
               credential: %{type: :email, username: "admin@admin.com", password: "adminpass"}
             })
  end

  test "you can't define a role in regular user creation" do
    assert {:ok, %User{role: "user"}} =
             Accounts.create_user(%{
               user: %{role: "admin", username: "1337H4xx0r"},
               credential: %{
                 type: :email,
                 username: "h4x@ur.router",
                 password: "correcthorsebatterystaple"
               }
             })
  end

  test "you can see if a username is available" do
    insert(:user, %{username: "taken"})

    assert false == Accounts.is_username_available?("taken")
    assert true = Accounts.is_username_available?("available")
  end

  test "you can delete a user" do
    {:ok, %User{} = user} = TestHelpers.create_random_user()

    assert {:ok, %User{}} = Accounts.delete_user(user)
  end

  test "you can get all users" do
    insert_pair(:user)
    users = Accounts.get_all_users()
    assert Enum.count(users) == 2
  end
end
