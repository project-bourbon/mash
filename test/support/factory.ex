defmodule Bourbon.Factory do
  use ExMachina.Ecto, repo: Bourbon.Repo
  alias Bourbon.{Threads, Accounts}

  def credential_factory do
    %Accounts.Credential{
      type: "email",
      username: Faker.Internet.email(),
      password_hash: Faker.String.base64(32)
    }
  end

  def profile_factory do
    %Accounts.Profile{
      name: Faker.Name.name(),
      location: Faker.StarWars.planet(),
      bio: Faker.Lorem.sentences() |> Enum.join(" ")
    }
  end

  def user_factory do
    %Accounts.User{
      username: Faker.Internet.user_name(),
      credentials: [build(:credential)],
      profile: build(:profile)
    }
  end

  def admin_user_factory do
    struct(user_factory(), %{
      role: "admin"
    })
  end

  def moderator_user_factory do
    struct(user_factory(), %{
      role: "moderator"
    })
  end

  def thread_factory do
    %Threads.Thread{
      user: build(:user),
      title: Faker.Lorem.sentence(),
      link: Faker.Internet.url(),
      privacy: "public",
      tags: Faker.Lorem.words()
    }
  end

  def comment_factory do
    %Threads.Comment{
      user: build(:user),
      thread: build(:thread),
      body: Faker.Lorem.paragraph()
    }
  end

  def reaction_factory do
    %Threads.Reaction{
      user: build(:user),
      thread: build(:thread),
      type: sequence(:type, ["emoji"]),
      identifier: Faker.Lorem.word()
    }
  end
end
