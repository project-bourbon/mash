defmodule BourbonWeb.AbsintheCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      use ExUnit.Case
      use Bourbon.DataCase, async: true
      import BourbonWeb.AbsintheCase
    end
  end

  def run(document, options \\ [], schema \\ BourbonWeb.Schema) do
    Absinthe.run(document, schema, options)
  end
end
