defmodule Bourbon.TestHelpers do
  alias Bourbon.{Accounts, Threads}

  def create_random_user do
    Accounts.create_user(%{
      user: %{username: Faker.Internet.user_name()},
      credential: %{type: :email, username: Faker.Internet.email(), password: Faker.Lorem.word()}
    })
  end

  def generate(:thread, amount \\ 1) do
    for _ <- 1..amount do
      {:ok, user} = create_random_user()

      Threads.create_thread(user, %{
        title: Faker.Lorem.sentence(),
        link: Faker.Internet.url(),
        privacy: :public,
        tags: Enum.map(1..3, fn _ -> Faker.Lorem.word() end)
      })
    end
  end
end
