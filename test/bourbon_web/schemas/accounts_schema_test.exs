defmodule BourbonWeb.Schemas.AccountSchemaTest do
  use BourbonWeb.AbsintheCase
  alias Bourbon.Accounts

  describe "mutations" do
    @query "
    mutation CreateUser(
      $username: String!
      $email: String!
      $password: String!
      $name: String
      $location: String
      $bio: String
    ) {
      createUser(
        username: $username
        profile: {
          name: $name
          location: $location
          bio: $bio
        }
        credential: {
          type: EMAIL
          username: $email
          password: $password
        }
      ) {
        id
        username
        profile {
          name
        }
        credentials {
          type
          username
        }
      }
    }
    "
    test "can create a user" do
      actual =
        run(@query,
          variables: %{
            "username" => "test1",
            "email" => "test@test.test",
            "password" => "password",
            "name" => "Test User",
            "location" => "USA",
            "bio" => "I'm just a test. Nothing real neat here."
          }
        )

      assert {:ok,
              %{
                data: %{
                  "createUser" => %{
                    "id" => id,
                    "username" => "test1",
                    "profile" => %{
                      "name" => "Test User"
                    },
                    "credentials" => [
                      %{
                        "type" => "email",
                        "username" => "test@test.test"
                      }
                    ]
                  }
                }
              }} = actual

      assert id != nil
      assert is_integer(id) == false
    end
  end

  @query "
    mutation Login(
      $email: String!
      $password: String!
    ) {
      login(email: $email, password: $password) {
        token
        user {
          username
        }
      }
    }
  "
  test "you can log in with your email and password" do
    insert(:user, %{
      username: "test1",
      credentials: [
        %Accounts.Credential{
          type: "email",
          username: "test@test.test",
          password_hash: Argon2.hash_pwd_salt("password")
        }
      ]
    })

    actual =
      run(@query,
        variables: %{
          "email" => "test@test.test",
          "password" => "password"
        }
      )

    assert {:ok,
            %{
              data: %{
                "login" => %{
                  "token" => _token,
                  "user" => %{
                    "username" => "test1"
                  }
                }
              }
            }} = actual
  end
end
