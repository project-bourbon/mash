defmodule BourbonWeb.Schemas.ThreadsTest do
  use BourbonWeb.AbsintheCase
  alias Bourbon.Crypto

  describe "queries" do
    @query "
      query {
        threadsSearch {
          title
        }
      }
    "
    test "can get threads" do
      insert_list(3, :thread)
      actual = run(@query)

      assert {:ok,
              %{
                data: %{
                  "threadsSearch" => [%{}, %{}, %{}]
                }
              }} = actual
    end

    @query "
      query {
        threadsSearch {
          insertedAt
        }
      }
    "
    test "can get a thread's inserted_at" do
      insert(:thread)
      actual = run(@query)

      assert {:ok,
              %{
                data: %{
                  "threadsSearch" => [%{"insertedAt" => actual_inserted_at}]
                }
              }} = actual

      assert {:ok, %DateTime{}, _utc_offset} = DateTime.from_iso8601(actual_inserted_at)
    end

    @query "
      query {
        threadsSearch(first: 2, from: 0) {
          title
        }
      }
    "
    test "can paginate threads" do
      insert_list(3, :thread)
      actual = run(@query)

      assert {:ok,
              %{
                data: %{
                  "threadsSearch" => [%{}, %{}]
                }
              }} = actual
    end

    @query "
      query TagThreads($tags: [String]) {
        threadsSearch(tags: $tags) {
          title
        }
      }
    "
    test "can get threads with a tag name" do
      user = insert(:user)

      insert(:thread,
        user: user,
        title: "Tag A Title",
        text: "Tag A Text",
        tags: ["TagA"],
        privacy: "public"
      )

      insert(:thread,
        user: user,
        title: "Tag B Title",
        text: "Tag B Text",
        tags: ["TagB"],
        privacy: "public"
      )

      actual =
        run(@query,
          variables: %{
            "tags" => ["TagB"]
          }
        )

      assert {:ok,
              %{
                data: %{
                  "threadsSearch" => [
                    %{
                      "title" => "Tag B Title"
                    }
                  ]
                }
              }} = actual
    end

    @query "
      query {
        threadsSearch {
          author {
            username
          }
        }
      }
    "
    test "can get the author from a thread" do
      user = insert(:user, username: "TestUser")

      insert(:thread,
        user: user,
        title: "Title",
        link: "https://example.com",
        tags: ["Tag"],
        privacy: "public"
      )

      actual = run(@query)

      assert {:ok,
              %{
                data: %{
                  "threadsSearch" => [
                    %{
                      "author" => %{
                        "username" => "TestUser"
                      }
                    }
                  ]
                }
              }} = actual
    end

    @query "
      query TagThreads($tags: [String]) {
        threadsSearch(tags: $tags) {
          title
        }
      }
    "
    test "can get threads with one of given tags" do
      user = insert(:user)

      ["A", "B", "C", "D", "AB", "DA"]
      |> Enum.each(fn l ->
        insert(:thread,
          user: user,
          title: "Tag #{l} Title",
          text: "Tag #{l} Text",
          tags: String.codepoints(l) |> Enum.map(&"Tag#{&1}"),
          privacy: "public"
        )
      end)

      actual =
        run(@query,
          variables: %{
            "tags" => ["TagB", "TagC"]
          }
        )

      assert {:ok,
              %{
                data: %{
                  "threadsSearch" => threads
                }
              }} = actual

      assert Enum.count(threads) == 3
      assert Enum.member?(threads, %{"title" => "Tag B Title"})
      assert Enum.member?(threads, %{"title" => "Tag C Title"})
      assert Enum.member?(threads, %{"title" => "Tag AB Title"})
    end
  end

  describe "mutations" do
    @query "
      mutation CreateThread(
        $title: String!
        $link: String!
        $privacy: PrivacyType!
        $tags: [String!]
      ) {
        createThread(
          title: $title
          link: $link
          privacy: $privacy
          tags: $tags
        ) {
          id
          title
          link
          tags
          privacy
        }
      }
    "
    test "can create a public thread with a link" do
      user = insert(:user)

      actual =
        run(@query,
          variables: %{
            "title" => "Test Title",
            "link" => "https://example.com",
            "tags" => ["tagA"],
            "privacy" => "PUBLIC"
          },
          context: %{current_user: user}
        )

      assert {:ok,
              %{
                data: %{
                  "createThread" => %{
                    "id" => _id,
                    "title" => "Test Title",
                    "link" => "https://example.com",
                    "tags" => ["tagA"],
                    "privacy" => "PUBLIC"
                  }
                }
              }} = actual
    end

    @query "
      mutation CreateThread(
        $title: String!
        $text: String!
        $privacy: PrivacyType!
        $tags: [String!]
      ) {
        createThread(
          title: $title
          text: $text
          privacy: $privacy
          tags: $tags
        ) {
          id
          title
          text
          tags
          privacy
        }
      }
    "
    test "can create a public thread with just text" do
      user = insert(:user)

      actual =
        run(@query,
          variables: %{
            "title" => "Test Title",
            "text" => "Howdy world!",
            "tags" => ["JustGirlyThings"],
            "privacy" => "PUBLIC"
          },
          context: %{current_user: user}
        )

      assert {:ok,
              %{
                data: %{
                  "createThread" => %{
                    "id" => _id,
                    "title" => "Test Title",
                    "text" => "Howdy world!",
                    "tags" => ["JustGirlyThings"],
                    "privacy" => "PUBLIC"
                  }
                }
              }} = actual
    end

    @query """
      mutation EditThread(
        $threadId: HashedID!
      ) {
        editThread(threadId: $threadId, changes: {
          title: "New Title"
          link: "https://example.new"
          text: "New Text"
          tags: ["NewTag"]
        }) {
          title
          link
          text
          tags
        }
      }
    """
    test "can edit a thread" do
      user = insert(:user)

      thread =
        insert(:thread,
          user: user,
          title: "Old Title",
          link: "https://example.old",
          text: "Old Body",
          tags: ["OldTag"]
        )

      actual =
        run(@query,
          variables: %{"threadId" => Crypto.encode(thread.id)},
          context: %{current_user: user}
        )

      assert {:ok,
              %{
                data: %{
                  "editThread" => %{
                    "title" => new_title,
                    "text" => new_text,
                    "link" => new_link,
                    "tags" => new_tags
                  }
                }
              }} = actual

      assert new_title == "New Title"
      assert new_text == "New Text"
      assert new_link == "https://example.new"
      assert new_tags == ["NewTag"]
    end

    @query "
      mutation DeleteThread($threadId: HashedID!) {
        deleteThread(threadId: $threadId) {
          id
        }
      }
    "
    test "can delete a thread" do
      user = insert(:user)
      thread = insert(:thread, user: user)

      actual =
        run(@query,
          variables: %{"threadId" => Crypto.encode(thread.id)},
          context: %{current_user: user}
        )

      assert {:ok, %{data: %{"deleteThread" => %{"id" => actual_id}}}} = actual
      assert Crypto.encode(thread.id) == actual_id
    end

    @query "
      mutation AddComment($threadId: HashedID!, $body: String!) {
        addComment(threadId: $threadId, body: $body) {
          body
          thread {
            id
          }
        }
      }
    "
    test "can add a comment to a thread" do
      user = insert(:user)
      thread = insert(:thread, user: user)

      actual =
        run(@query,
          variables: %{"threadId" => Crypto.encode(thread.id), "body" => "Sup!"},
          context: %{current_user: user}
        )

      assert {:ok,
              %{data: %{"addComment" => %{"body" => "Sup!", "thread" => %{"id" => thread_id}}}}} =
               actual

      assert Crypto.encode(thread.id) == thread_id
    end

    @query "
      mutation EditComment($commentId: HashedID!, $body: String!) {
        editComment(commentId: $commentId, body: $body) {
          body
        }
      }
    "
    test "can edit a comment" do
      user = insert(:user)
      thread = insert(:thread, user: user)
      comment = insert(:comment, user: user, thread: thread, body: "Old!")

      actual =
        run(@query,
          variables: %{"commentId" => Crypto.encode(comment.id), "body" => "New!"},
          context: %{current_user: user}
        )

      assert {:ok,
              %{
                data: %{
                  "editComment" => %{
                    "body" => "New!"
                  }
                }
              }} = actual
    end

    @query "
      mutation DeleteComment($commentId: HashedID!) {
        deleteComment(commentId: $commentId) {
          id
        }
      }
    "
    test "can delete a comment" do
      user = insert(:user)
      thread = insert(:thread, user: user)
      comment = insert(:comment, user: user, thread: thread)

      actual =
        run(@query,
          variables: %{"commentId" => Crypto.encode(comment.id)},
          context: %{current_user: user}
        )

      assert {:ok,
              %{
                data: %{
                  "deleteComment" => %{
                    "id" => actual_id
                  }
                }
              }} = actual
    end

    @query "
      mutation ArchiveThread($threadId: HashedID!) {
        archiveThread(threadId: $threadId) {
          id
          archivedAt
        }
      }
    "
    test "can't archive a thread as a regular user" do
      user = insert(:user)
      thread = insert(:thread, user: user)

      actual =
        run(@query,
          variables: %{"threadId" => Crypto.encode(thread.id)},
          context: %{current_user: user}
        )

      assert {:ok, %{errors: [%{message: "unauthorized"}]}} = actual
    end

    test "can archive a thread as a moderator" do
      user = insert(:moderator_user)
      creating_user = insert(:user)
      %{id: thread_id} = thread = insert(:thread, user: creating_user)

      actual =
        run(@query,
          variables: %{"threadId" => Crypto.encode(thread.id)},
          context: %{current_user: user}
        )

      assert {:ok,
              %{
                data: %{
                  "archiveThread" => %{
                    "archivedAt" => archived_at,
                    "id" => actual_id
                  }
                }
              }} = actual

      assert {:ok, %DateTime{}, _} = DateTime.from_iso8601(archived_at)
      assert {:ok, ^thread_id} = Crypto.decode(actual_id)
    end

    test "can archive a thread as an admin" do
      user = insert(:admin_user)
      creating_user = insert(:user)
      %{id: thread_id} = thread = insert(:thread, user: creating_user)

      actual =
        run(@query,
          variables: %{"threadId" => Crypto.encode(thread.id)},
          context: %{current_user: user}
        )

      assert {:ok,
              %{
                data: %{
                  "archiveThread" => %{
                    "archivedAt" => archived_at,
                    "id" => actual_id
                  }
                }
              }} = actual

      assert {:ok, %DateTime{}, _} = DateTime.from_iso8601(archived_at)
      assert {:ok, ^thread_id} = Crypto.decode(actual_id)
    end
  end
end
