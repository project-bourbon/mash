{:ok, _} = Application.ensure_all_started(:ex_machina)

ExUnit.start()

Ecto.Adapters.SQL.Sandbox.mode(Bourbon.Repo, :manual)

Absinthe.Test.prime(BourbonWeb.Schema)
