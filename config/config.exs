# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :bourbon, ecto_repos: [Bourbon.Repo], hashid_salt: "6rMNyjmoChhsSVAUXyd"

config :bourbon, Bourbon.Repo,
  adapter: Ecto.Adapters.Postgres,
  pool_size: String.to_integer(System.get_env("POOL_SIZE") || "10")

# Configures the endpoint
config :bourbon, BourbonWeb.Endpoint,
  secret_key_base: "TDDNVPAVNoELOczZop3f8L32i++Ux8wnp2ZRgODPT65SKmDaVv0ld/Ugznasw3dO",
  render_errors: [view: BourbonWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Bourbon.PubSub, adapter: Phoenix.PubSub.PG2]

config :bourbon, Bourbon.Guardian,
  allowed_algos: ["HS512"],
  verify_module: Guardian.JWT,
  issuer: "Seent",
  ttl: {30, :days},
  verify_issuer: true,
  secret_key: "5xw/tTvZ/HJSRdJpZ/D4sLwWg5VU1hAaXvLrQU47Zv1udTZvkXJ5Zqr0rGcy+YPJ"

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
