use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :bourbon, BourbonWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Drastically decrease argon2's complexity (AND SECURITY!)
# only during tests.
config :argon2_elixir, t_cost: 1, m_cost: 8

# Configure your database
config :bourbon, Bourbon.Repo,
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  username: "postgres",
  password: "postgres",
  database: "bourbon_test",
  pool: Ecto.Adapters.SQL.Sandbox
