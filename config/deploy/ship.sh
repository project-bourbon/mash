#!/bin/bash

# If any of this commands fail, stop script.
set -e

AWS_ACCESS_KEY_ID="$AWS_ACCESS_KEY_ID"
AWS_SECRET_ACCESS_KEY="$AWS_SECRET_ACCESS_KEY"
AWS_DEFAULT_REGION="us-east-1"

# Set AWS ECS vars.
# Here you only need to set AWS_ECS_URL. I have created the others so that
# it's easy to change for a different project. AWS_ECS_URL should be the
# base url.
AWS_ECS_URL="352354857413.dkr.ecr.us-east-1.amazonaws.com"
AWS_ECS_PROJECT_NAME="bourbon-mash"
AWS_ECS_CONTAINER_NAME="mash"
AWS_ECS_DOCKER_IMAGE="mash:latest"
AWS_ECS_CLUSTER_NAME="bourbon-mash-production"

# Early exit
function command_exists() {
  type "$1" &> /dev/null ;
}

function early_exit() {
  echo "$1 isn't defined"
  exit 1;
}

# Check programs
if ! command_exists docker ; then
    early_exit "Docker"
fi
if ! command_exists aws ; then
    early_exit "aws"
fi
if ! command_exists ecs-cli ; then
    early_exit "ecs"
fi
if ! command_exists jq ; then
    early_exit "jq"
fi

# Check Environment variables
if [ "$AWS_ACCESS_KEY_ID" = "" ]; then
    early_exit "AWS_ACCESS_KEY_ID"
fi
if [ "$AWS_SECRET_ACCESS_KEY" = "" ]; then
    early_exit "AWS_SECRET_ACCESS_KEY"
fi
if [ "$DB_USERNAME" = "" ]; then
    early_exit "DB_USERNAME"
fi
if [ "$DB_PASSWORD" = "" ]; then
    early_exit "DB_PASSWORD"
fi
if [ "$SECRET_KEY_BASE" = "" ]; then
    early_exit "SECRET_KEY_BASE"
fi
if [ "$HASHID_SECRET" = "" ]; then
    early_exit "HASHID_SECRET"
fi
if [ "$TIMBER_LOGS_KEY" = ""]; then
    early_exit "TIMBER_LOGS_KEY"
fi

# Set Build args.
# These are the build arguments we used before.
# Note that the DATABASE_URL needs to be set.
DB_HOSTNAME=bourbon-production.c6twzfrgzvsa.us-east-1.rds.amazonaws.com
SECRET_KEY_BASE=$SECRET_KEY_BASE

# Set runtime ENV.
# These are the runtime environment variables.
# Note that HOST needs to be set.
HOST=$HOST
PORT=4000

### Some debug stuff

echo "AWS_ECS_CONTAINER_NAME: $AWS_ECS_CONTAINER_NAME"
echo "AWS_ECS_URL: $AWS_ECS_URL"
echo "AWS_ECS_DOCKER_IMAGE: $AWS_ECS_DOCKER_IMAGE"
echo "AWS_ECS_CLUSTER_NAME: $AWS_ECS_CLUSTER_NAME"
echo "AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION"

### Resume work :)

# Build docker-compose.yml with our configuration.
# Here we are going to replace the docker-compose.yml placeholders with
# our app's configurations
sed \
  -i'.bak' \
  -e 's/$AWS_ECS_URL/'$AWS_ECS_URL'/g' \
  -e 's/$AWS_ECS_DOCKER_IMAGE/'$AWS_ECS_DOCKER_IMAGE'/g' \
  -e 's/$AWS_ECS_CONTAINER_NAME/'$AWS_ECS_CONTAINER_NAME'/g' \
  -e 's/$HOST/'$HOST'/g' \
  -e 's/$PORT/'$PORT'/g' \
  -e 's/$DB_HOSTNAME/'$DB_HOSTNAME'/g' \
  -e 's/$DB_USERNAME/'$DB_USERNAME'/g' \
  -e 's/$DB_PASSWORD/'$DB_PASSWORD'/g' \
  -e 's/$CI_JOB_ID/'$CI_JOB_ID'/g' \
  -e 's/$CI_PIPELINE_ID/'$CI_PIPELINE_ID'/g' \
  -e 's/$CI_COMMIT_SHA/'$CI_COMMIT_SHA'/g' \
  -e 's/$CI_COMMIT_TAG/'$CI_COMMIT_TAG'/g' \
  -e 's/$CI_ENVIRONMENT_NAME/'$CI_ENVIRONMENT_NAME'/g' \
  -e 's/$HASHID_SECRET/'$HASHID_SECRET'/g' \
  -e 's/$TIMBER_LOGS_KEY/'$TIMBER_LOGS_KEY'/g' \
  config/deploy/docker-compose.yml

# Build container.
# As we did before, but now we are going to build the Docker image that will
# be pushed to the repository.
docker build --pull -t $AWS_ECS_CONTAINER_NAME \
  --build-arg SECRET_KEY_BASE=$SECRET_KEY_BASE \
  --build-arg DB_HOSTNAME=$DB_HOSTNAME \
  --build-arg DB_USERNAME=$DB_USERNAME \
  --build-arg DB_PASSWORD=$DB_PASSWORD \
  --build-arg HASHID_SECRET=$HASHID_SECRET \
  .

# Tag the new Docker image as latest on the ECS Repository.
docker tag $AWS_ECS_DOCKER_IMAGE "$AWS_ECS_URL"/"$AWS_ECS_DOCKER_IMAGE"

# Login to ECS Repository.
eval $(aws ecr get-login --no-include-email --region $AWS_DEFAULT_REGION)

# Upload the Docker image to the ECS Repository.
docker push "$AWS_ECS_URL"/"$AWS_ECS_DOCKER_IMAGE"

# Configure ECS cluster and AWS_DEFAULT_REGION so we don't have to send it
# on every command
ecs-cli configure --cluster=$AWS_ECS_CLUSTER_NAME --region=$AWS_DEFAULT_REGION

# Deregister old task definition.
# Every deploy we want a new task definition to be created with the latest
# configurations. Task definitions are a set of configurations that state
# how the Docker container should run and what resources to use: https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definitions.html
REVISION=$(aws ecs list-task-definitions --region $AWS_DEFAULT_REGION | jq '.taskDefinitionArns[]' | tr -d '"' | tail -1 | rev | cut -d':' -f 1 | rev)
if [ ! -z "$REVISION" ]; then
  aws ecs deregister-task-definition \
    --region $AWS_DEFAULT_REGION \
    --task-definition $AWS_ECS_PROJECT_NAME:$REVISION \
    >> /tmp/task_output_app.txt

  # Stop current task that is running ou application.
  # This is what will stop the application.
  ecs-cli compose \
    --file config/deploy/docker-compose.yml \
    --project-name "$AWS_ECS_PROJECT_NAME" \
    service stop
fi

# Start new task which will create fresh new task definition as well.
# This is what brings the application up with the new changes and configurations.
ecs-cli compose \
  --file config/deploy/docker-compose.yml \
  --project-name "$AWS_ECS_PROJECT_NAME" \
  service up
