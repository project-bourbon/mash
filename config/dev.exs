use Mix.Config

config :bourbon, BourbonWeb.Endpoint,
  http: [port: 4000],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: []

config :logger, :console, format: "[$level] $message\n"

config :phoenix, :stacktrace_depth, 20

# Configure your database
config :bourbon, Bourbon.Repo,
  hostname: "localhost",
  username: "postgres",
  password: "postgres",
  database: "bourbon_dev"
