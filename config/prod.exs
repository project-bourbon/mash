use Mix.Config

config :bourbon, hashid_salt: System.get_env("HASHID_SECRET")

config :bourbon, BourbonWeb.Endpoint,
  load_from_system_env: true,
  http: [port: {:system, "PORT"}],
  server: true,
  version: Application.spec(:bourbon, :vsn),
  secret_key_base: System.get_env("SECRET_KEY_BASE")

config :bourbon, Bourbon.Repo,
  hostname: System.get_env("DB_HOSTNAME"),
  username: System.get_env("DB_USERNAME"),
  password: System.get_env("DB_PASSWORD"),
  database: "bourbon"

config :bourbon, Bourbon.Guardian,
  allowed_algos: ["HS512"],
  verify_module: Guardian.JWT,
  issuer: "Seent",
  ttl: {30, :days},
  verify_issuer: true,
  secret_key: System.get_env("SECRET_KEY_BASE")

# Do not print debug messages in production
config :logger, level: :info

# Import Timber, structured logging
import_config "timber.exs"
