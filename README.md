# Mash

### What is it?

The core service layer for Project Bourbon.

### Build Requirements

* [jq](https://stedolan.github.io/jq/download/)
* docker
* aws-cli
* ecs-cli

For production builds you need the following environment variables:

`AWS_ACCESS_KEY_ID`
`AWS_SECRET_ACCESS_KEY`
`DB_USERNAME`
`DB_PASSWORD`
`SECRET_KEY_BASE`
`HASHID_SECRET`

```sh
AWS_ACCESS_KEY_ID="" \
AWS_SECRET_ACCESS_KEY="" \
DB_USERNAME="" \
DB_PASSWORD="" \
SECRET_KEY_BASE="" \
HASHID_SECRET="" \
sh config/deploy/shipit.sh
```

### Run Production Locally

```sh
docker build -t mash:latest \
  --build-arg SECRET_KEY_BASE=super_secret_key_base \
  --build-arg DB_HOSTNAME=docker.for.mac.host.internal \
  --build-arg DB_USERNAME=postgres \
  --build-arg DB_PASSWORD=postgres \
  --build-arg DB_PASSWORD=postgres \
  --build-arg HASHID_SECRET=super_secret_secret \
  .
```

```sh
docker run --rm -it -p 4001:4001 \
  -e PORT=4001 \
  -e DB_HOSTNAME=docker.for.mac.host.internal \ # May have to use localhost if not on macOS
  -e DB_USERNAME=postgres \
  -e DB_PASSWORD=postgres \
  -e HASHID_SECRET=super_secret_secret
  mash:latest
```
