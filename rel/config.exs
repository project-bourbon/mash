Path.join(["rel", "plugins", "*.exs"])
|> Path.wildcard()
|> Enum.map(&Code.eval_file(&1))

use Mix.Releases.Config,
  default_release: :default,
  default_environment: Mix.env()

environment :dev do
  set(dev_mode: true)
  set(include_erts: false)
  set(cookie: :"xbu{kJAH1Tu16MKfK7}^ANuPo[CcuMuT9m=.BERLB.]w]>VSN]tlQkA9jZHUa[<w")
end

environment :prod do
  set(include_erts: true)
  set(include_src: false)
  set(cookie: :"Sg:@;wVB4pXh(5<K}z&tk4(3|hss@(XUkSUo;usQpWM,!8q68$AAJPXV_Ad{`Kv]")
end

release :mash do
  set(applications: [:runtime_tools, bourbon: :permanent])
  set(version: current_version(:bourbon))
  set(pre_start_hook: "rel/hooks/pre_start.sh")
end
