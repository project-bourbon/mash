defmodule BourbonWeb.Middleware.Authentication do
  @moduledoc """
  Absinthe middleware that will make it required for a user
  to be logged in.
  """

  @behaviour Absinthe.Middleware

  @impl Absinthe.Middleware
  def call(resolution = %{context: %{current_user: _user}}, _config) do
    resolution
  end

  @impl Absinthe.Middleware
  def call(resolution, _config) do
    resolution |> Absinthe.Resolution.put_result({:error, "unauthenticated"})
  end
end
