defmodule BourbonWeb.Context do
  @moduledoc """
  Plug to get user from Authorization header via Guardian.
  """

  @behaviour Plug

  import Plug.Conn
  alias Bourbon.Guardian

  @impl true
  def init(opts), do: opts

  @impl true
  def call(conn, _) do
    case get_req_header(conn, "authorization") do
      ["Bearer " <> token] ->
        case Guardian.resource_from_token(token) do
          {:ok, {:ok, user}, _claims} ->
            Absinthe.Plug.put_options(conn, context: %{current_user: user})

          _ ->
            conn
        end

      _ ->
        conn
    end
  end
end
