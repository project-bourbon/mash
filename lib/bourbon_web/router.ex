defmodule BourbonWeb.Router do
  use BourbonWeb, :router
  use Plug.ErrorHandler

  pipeline :api do
    plug(Guardian.Plug.LoadResource, allow_blank: true)
    plug(BourbonWeb.Context)
  end

  pipeline :logger do
    plug(Timber.Integrations.HTTPContextPlug)
    plug(Timber.Integrations.EventPlug)
  end

  scope "/", BourbonWeb do
    get("/info", PageController, :info)
    get("/health", PageController, :health)
  end

  scope "/" do
    pipe_through(:api)
    pipe_through(:logger)
    forward("/graphql", Absinthe.Plug, schema: BourbonWeb.Schema)

    forward(
      "/graphiql",
      Absinthe.Plug.GraphiQL,
      schema: BourbonWeb.Schema,
      interface: :playground,
      socket: BourbonWeb.UserSocket
    )
  end
end
