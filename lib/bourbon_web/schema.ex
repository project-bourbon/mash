defmodule BourbonWeb.Schema do
  @moduledoc """
  Schema module for Absinthe. Creates a GraphQL interface and defines
  all GraphQL queries, mutations, and subscriptions.
  """

  use Absinthe.Schema
  alias Bourbon.{Accounts, Threads}
  alias BourbonWeb.Schemas.{Shared, AccountSchema, ThreadSchema}

  def context(cxt) do
    loader =
      Dataloader.new()
      |> Dataloader.add_source(:threads, Threads.data())
      |> Dataloader.add_source(:accounts, Accounts.data())

    Map.put(cxt, :loader, loader)
  end

  def plugins do
    [Absinthe.Middleware.Dataloader | Absinthe.Plugin.defaults()]
  end

  import_types(Absinthe.Type.Custom)
  import_types(Shared)
  import_types(AccountSchema)
  import_types(ThreadSchema)

  query do
    import_fields(:account_queries)
    import_fields(:thread_queries)
  end

  mutation do
    import_fields(:account_mutations)
    import_fields(:thread_mutations)
  end
end
