defmodule BourbonWeb.Schemas.Shared do
  @moduledoc """
  Absinthe Schema for generic/common types that are shared
  among other schemas.
  """

  use Absinthe.Schema.Notation

  scalar :hashed_id, name: "HashedID" do
    serialize(&serialize_id/1)
    parse(&parse_hashed_id/1)
  end

  defp parse_hashed_id(%Absinthe.Blueprint.Input.String{value: value}) when is_binary(value) do
    case Bourbon.Crypto.decode(value) do
      {:ok, id} -> {:ok, id}
      _ -> :error
    end
  end

  defp parse_hashed_id(_), do: :error

  defp serialize_id(value) when is_binary(value) do
    value
  end
end
