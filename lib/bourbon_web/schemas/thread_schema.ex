defmodule BourbonWeb.Schemas.ThreadSchema do
  @moduledoc """
  Absinthe schema for Threads.
  """
  use Absinthe.Schema.Notation
  import Absinthe.Resolution.Helpers, only: [dataloader: 3]
  alias BourbonWeb.Resolvers.{ThreadsResolver, SharedResolver}

  enum :privacy_type do
    value(:public)
  end

  input_object :thread_editables do
    field(:title, :string)
    field(:link, :string)
    field(:text, :string)
    field(:privacy, :string)
    field(:tags, list_of(:string))
  end

  object :thread do
    field(:id, non_null(:hashed_id), resolve: &SharedResolver.encode_db_id/3)
    field(:title, non_null(:string))
    field(:link, :string)
    field(:text, :string)
    field(:privacy, non_null(:privacy_type))
    field(:tags, non_null(list_of(:string)))
    field(:author, non_null(:account), resolve: dataloader(:threads, :user, []))
    field(:inserted_at, non_null(:datetime))
    field(:updated_at, non_null(:datetime))
    field(:archived_at, :datetime)
  end

  object :comment do
    field(:id, non_null(:hashed_id), resolve: &SharedResolver.encode_db_id/3)
    field(:body, non_null(:string))
    field(:author, non_null(:account), resolve: dataloader(:threads, :user, []))
    field(:inserted_at, non_null(:datetime))
    field(:updated_at, non_null(:datetime))
    field(:thread, non_null(:thread), resolve: dataloader(:threads, :thread, []))
  end

  object :thread_queries do
    field :threads_search, list_of(:thread) do
      # TODO
      # Make this return an object with a `nodes` field
      # And have it give some meta information. Look at
      # https://db-api.destinytracker.com/api/graphql for an example
      # "itemsSearch" query

      # TODO: Add sortBy
      # TODO: Add sortDirection
      arg(:first, :integer)
      arg(:from, :integer)
      arg(:tags, list_of(:string))
      resolve(&ThreadsResolver.threads/3)
    end
  end

  object :thread_mutations do
    field :create_thread, :thread do
      middleware(BourbonWeb.Middleware.Authentication)
      arg(:title, non_null(:string))
      arg(:link, :string)
      arg(:text, :string)
      arg(:privacy, non_null(:privacy_type))
      arg(:tags, non_null(list_of(:string)))
      resolve(&ThreadsResolver.create_thread/3)
    end

    field :edit_thread, :thread do
      middleware(BourbonWeb.Middleware.Authentication)
      arg(:thread_id, non_null(:hashed_id))
      arg(:changes, non_null(:thread_editables))
      resolve(&ThreadsResolver.edit_thread/3)
    end

    field :delete_thread, :thread do
      middleware(BourbonWeb.Middleware.Authentication)
      arg(:thread_id, non_null(:hashed_id))
      resolve(&ThreadsResolver.delete_thread/3)
    end

    field :add_comment, :comment do
      middleware(BourbonWeb.Middleware.Authentication)
      arg(:thread_id, non_null(:hashed_id))
      arg(:body, non_null(:string))
      resolve(&ThreadsResolver.add_comment/3)
    end

    field :edit_comment, :comment do
      middleware(BourbonWeb.Middleware.Authentication)
      arg(:comment_id, non_null(:hashed_id))
      arg(:body, non_null(:string))
      resolve(&ThreadsResolver.edit_comment/3)
    end

    field :delete_comment, :comment do
      middleware(BourbonWeb.Middleware.Authentication)
      arg(:comment_id, non_null(:hashed_id))
      resolve(&ThreadsResolver.delete_comment/3)
    end

    field :archive_thread, :thread do
      middleware(BourbonWeb.Middleware.Authentication)
      arg(:thread_id, non_null(:hashed_id))
      resolve(&ThreadsResolver.archive_thread/3)
    end

    # TODO: react_to_thread
    # TODO: remove_thread_reaction
  end
end
