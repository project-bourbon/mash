defmodule BourbonWeb.Schemas.AccountSchema do
  use Absinthe.Schema.Notation
  import Absinthe.Resolution.Helpers, only: [dataloader: 1, dataloader: 3]
  alias BourbonWeb.Resolvers.{AccountsResolver, SharedResolver}

  @moduledoc """
  Absinthe Schema for Accounts.
  """

  @desc "A user account"
  object :account do
    field(
      :id,
      non_null(:string),
      resolve: &SharedResolver.encode_db_id/3
    )

    field(:username, non_null(:string))
    field(:profile, :account_profile, resolve: dataloader(:accounts))
  end

  object :me do
    field(
      :id,
      non_null(:string),
      resolve: &SharedResolver.encode_db_id/3
    )

    field(:username, non_null(:string))
    field(:credentials, list_of(:account_credential), resolve: dataloader(:accounts))
    field(:profile, :account_profile, resolve: dataloader(:accounts))
    field(:created_at, non_null(:datetime), resolve: dataloader(:accounts, :inserted_at, []))
    field(:updated_at, non_null(:datetime))
  end

  object :account_credential do
    field(:type, non_null(:string))
    field(:username, non_null(:string))
    field(:created_at, non_null(:datetime), resolve: dataloader(:accounts, :inserted_at, []))
    field(:updated_at, non_null(:datetime))
  end

  object :account_profile do
    field(:name, :string)
    field(:location, :string)
    field(:birthdate, :datetime)
    field(:bio, :string)
  end

  object :login_object do
    field(:token, :string)
    field(:user, :account)
  end

  input_object :credential_creation_object do
    field(:type, non_null(:credential_type))
    field(:username, non_null(:string))
    field(:password, :string)
  end

  enum :credential_type do
    value(:email)
    # More later??
  end

  input_object :account_creation_profile_object do
    import_fields(:account_profile)
  end

  object :account_queries do
    @desc "Get all users"
    field :all_users, list_of(:account) do
      deprecate("This is only meant for demonstration the graphql API. Will remove later.")
      resolve(&AccountsResolver.all_users/3)
    end
  end

  object :account_mutations do
    @desc "Create a user"
    field :create_user, :me do
      arg(:username, non_null(:string))
      arg(:credential, non_null(:credential_creation_object))
      arg(:profile, :account_creation_profile_object)
      resolve(&AccountsResolver.create_user/3)
    end

    @desc "Login with your email and password"
    field :login, :login_object do
      arg(:email, non_null(:string))
      arg(:password, non_null(:string))
      resolve(&AccountsResolver.login_with_email/3)
    end
  end
end
