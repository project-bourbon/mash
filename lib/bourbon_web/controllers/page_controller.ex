defmodule BourbonWeb.PageController do
  @moduledoc """
  Basic JSON pages that display helpful info.
  """

  use BourbonWeb, :controller

  @build_date DateTime.utc_now() |> DateTime.to_iso8601()

  @doc """
  Displays basic info about the API.
  """
  def info(conn, _params) do
    conn
    |> json(%{
      build_date: @build_date,
      build_job_id: System.get_env("CI_JOB_ID"),
      build_pipeline_id: System.get_env("CI_PIPELINE_ID"),
      commit_sha: System.get_env("CI_COMMIT_SHA"),
      environment: System.get_env("CI_ENVIRONMENT_NAME")
    })
  end

  @doc """
  Basic health-check endpoint.
  """
  def health(conn, _params) do
    conn
    |> json(%{healthy: true})
  end
end
