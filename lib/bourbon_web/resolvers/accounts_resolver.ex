defmodule BourbonWeb.Resolvers.AccountsResolver do
  @moduledoc """
  Absinthe resolvers for the Accounts module.
  """

  alias Bourbon.{Guardian, Accounts}
  alias Bourbon.Accounts.User
  import BourbonWeb.Helpers

  def all_users(_root, _args, _info) do
    users = Accounts.get_all_users()
    {:ok, users}
  end

  def create_user(_root, args, _info) do
    username = Map.get(args, :username)
    user_attrs = %{username: username}
    args = args |> Map.drop([:username]) |> Map.put(:user, user_attrs)

    case Accounts.create_user(args) do
      {:error, _error_name, changeset = %Ecto.Changeset{}, _changes} ->
        {:error, message: changeset_to_absinthe_error_message(changeset)}

      {:ok, user} ->
        {:ok, user}
    end
  end

  def login_with_email(_root, args, _info) do
    with {:ok, user = %User{}} <- Accounts.login_with_email(args),
         {:ok, jwt, _} <- encode_and_sign(user) do
      {:ok, %{token: jwt, user: user}}
    else
      {:error, :not_found} ->
        {:error, message: "Invalid username or password."}
    end
  end

  defp encode_and_sign(user = %User{}) do
    Guardian.encode_and_sign(user, %{}, token_type: "access")
  end
end
