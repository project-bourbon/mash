defmodule BourbonWeb.Resolvers.SharedResolver do
  @moduledoc """

  """

  alias Bourbon.Crypto

  def encode_db_id(%{id: id}, _args, _info) do
    {:ok, Crypto.encode(id)}
  end
end
