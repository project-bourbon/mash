defmodule BourbonWeb.Resolvers.ThreadsResolver do
  @moduledoc """
  Absinthe resolvers for the Threads module.
  """

  alias Bourbon.Threads

  def threads(_root, args, _info) do
    args =
      args
      |> Map.put_new(:first, 10)
      |> Map.put_new(:from, 0)
      |> Map.put_new(:tags, [])

    opts = [first: args.first, from: args.from, tags: args.tags]
    {:ok, Threads.get_threads(opts)}
  end

  def create_thread(_root, args, %{context: %{current_user: user}}) do
    Threads.create_thread(user, args)
    |> convert_privacy_to_atom
  end

  def edit_thread(_root, args, %{context: %{current_user: user}}) do
    thread = Threads.get_thread_by_id(args.thread_id)
    Threads.edit_thread(thread, user, args.changes)
  end

  def delete_thread(_root, args, %{context: %{current_user: user}}) do
    thread = Threads.get_thread_by_id(args.thread_id)
    Threads.delete_thread(thread, user)
  end

  def add_comment(_root, args, %{context: %{current_user: user}}) do
    thread = Threads.get_thread_by_id(args.thread_id)
    Threads.create_comment(user, thread, %{body: args.body})
  end

  def edit_comment(_root, args, %{context: %{current_user: user}}) do
    comment = Threads.get_comment_by_id(args.comment_id)
    Threads.edit_comment(comment, user, %{body: args.body})
  end

  def delete_comment(_root, %{comment_id: id}, %{context: %{current_user: user}}) do
    comment = Threads.get_comment_by_id(id)
    Threads.delete_comment(comment, user)
  end

  def archive_thread(_root, %{thread_id: id}, %{context: %{current_user: user}}) do
    thread = Threads.get_thread_by_id(id)
    Threads.archive_thread(thread, user)
  end

  def convert_privacy_to_atom(passed_value) do
    case passed_value do
      {:ok, thread = %Threads.Thread{}} ->
        thread = thread |> Map.put(:privacy, String.to_atom(thread.privacy))
        {:ok, thread}

      _ ->
        passed_value
    end
  end
end
