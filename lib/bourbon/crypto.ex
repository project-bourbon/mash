defmodule Bourbon.Crypto do
  @moduledoc """
  Helper functions surrounding hashids.

  The main purpose of this is to make our IDs not _necessarily_ sequential (at least, exposed that way).
  For example:

  Imagine seeing `/users/1` in the browser. You might be able to assume that you can get another user
  by going to `/users/2` and you'd probably be right (if there are two users). I'd prefer not to do that,
  so we use Hash IDs, which hash these numbers and are reversable so long as you have a key. Now they look like this:

  `/users/LQKK0M7Ob` and `/users/Vg6d7MGd3` instead of `/users/1` and `/users/2`,
  making it a bit less appearing as if they're sequential.

  Let it be clear: There is _zero_ intended security benefit, it just makes it not "feel"
  sequential and makes it just obscure actual counts of threads/users and eliminates an easy
  way to just visit each user/thread by bumping a number. And it looks nicer, IMO.
  """

  @s Hashids.new(salt: Application.get_env(:bourbon, :hashid_salt), min_len: 10)

  @doc "Takes a single number and encodes it"
  @spec encode(number) :: String.t()
  def encode(id) when is_integer(id) and id >= 0 do
    Hashids.encode(@s, id)
  end

  @doc "Decodes a hashid that contains a single number"
  @spec decode(String.t()) :: number
  def decode(hashed_id) do
    case Hashids.decode(@s, hashed_id) do
      {:ok, [id]} -> {:ok, id}
      {:error, :invalid_input_data} -> {:error, :bad_hashid}
    end
  end
end
