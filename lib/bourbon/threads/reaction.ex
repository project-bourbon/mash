defmodule Bourbon.Threads.Reaction do
  use Ecto.Schema
  import Ecto.Changeset
  alias Bourbon.Accounts.User
  alias Bourbon.Threads.Thread
  alias __MODULE__

  schema "reactions" do
    belongs_to(:user, User)
    belongs_to(:thread, Thread)
    field(:type, :string, null: false)
    field(:identifier, :string, null: false)
  end

  def changeset(reaction = %Reaction{}, attrs) do
    reaction
    |> cast(attrs, [:user_id, :type, :identifier])
    |> validate_required([:user_id, :type, :identifier])
  end

  def new_changeset(reaction = %Reaction{}, user = %User{}, thread = %Thread{}, attrs) do
    attrs = Map.merge(attrs, %{user_id: user.id, thread_id: thread.id})

    reaction
    |> changeset(attrs)
  end
end
