defmodule Bourbon.Threads do
  @moduledoc """
  Functions for interfacing with threads.
  """

  @behaviour Bodyguard.Policy

  import Ecto.Query
  alias Bourbon.Repo
  alias Bourbon.Accounts.User
  alias Bourbon.Threads.{Thread, Comment, Reaction}
  alias __MODULE__

  def data() do
    Dataloader.Ecto.new(Repo, query: &query/2)
  end

  def query(queryable, _params), do: queryable

  def authorize(_action, %User{role: role}, _resource) when role in ["admin", "moderator"],
    do: true

  def authorize(:create_comment, %User{role: role}, %Thread{archived_at: archived_at})
      when role == "user" and is_nil(archived_at),
      do: true

  def authorize(:react_to_thread, %User{role: role}, %Thread{archived_at: archived_at})
      when role == "user" and is_nil(archived_at),
      do: true

  def authorize(action, %User{id: user_id}, %Thread{user_id: user_id})
      when action in [:edit_thread, :delete_thread],
      do: true

  def authorize(action, %User{id: user_id}, %Comment{user_id: user_id})
      when action in [:edit_comment, :delete_comment],
      do: true

  def authorize(:remove_reaction, %User{id: user_id}, %Reaction{user_id: user_id}), do: true

  def authorize(_, _, _), do: false

  def get_threads(opts) do
    query =
      from(t in Thread)
      |> get_thread_with_tags(Keyword.get(opts, :tags))
      |> limit(^Keyword.get(opts, :first))
      |> offset(^Keyword.get(opts, :last))

    Repo.all(query)
  end

  def get_thread_by_id(id) do
    Repo.get(Thread, id)
  end

  def create_thread(user = %User{}, attrs) do
    user
    |> Ecto.build_assoc(:threads)
    |> Thread.new_changeset(attrs)
    |> Repo.insert()
  end

  def delete_thread(thread = %Thread{}, user = %User{}) do
    case Bodyguard.permit(Threads, :delete_thread, user, thread) do
      :ok ->
        thread
        |> Repo.delete()

      {:error, reason} ->
        {:error, reason}
    end
  end

  def edit_thread(thread = %Thread{}, user = %User{}, attrs) do
    case Bodyguard.permit(Threads, :edit_thread, user, thread) do
      :ok ->
        thread
        |> Thread.edit_changeset(attrs)
        |> Repo.update()

      {:error, reason} ->
        {:error, reason}
    end
  end

  def create_comment(user = %User{}, thread = %Thread{}, attrs) do
    case Bodyguard.permit(Threads, :create_comment, user, thread) do
      :ok ->
        attrs =
          attrs
          |> Map.put(:user_id, user.id)
          |> Map.put(:thread_id, thread.id)

        %Comment{}
        |> Comment.new_changeset(attrs)
        |> Repo.insert()

      err ->
        err
    end
  end

  def get_comment_by_id(id) do
    Repo.get(Comment, id)
  end

  def edit_comment(comment = %Comment{}, user = %User{}, attrs) do
    case Bodyguard.permit(Threads, :edit_comment, user, comment) do
      :ok ->
        comment
        |> Comment.edit_changeset(attrs)
        |> Repo.update()

      {:error, reason} ->
        {:error, reason}
    end
  end

  def delete_comment(comment = %Comment{}, user = %User{}) do
    case Bodyguard.permit(Threads, :delete_comment, user, comment) do
      :ok ->
        comment
        |> Repo.delete()

      {:error, reason} ->
        {:error, reason}
    end
  end

  def react_to_thread(user = %User{}, thread = %Thread{}, attrs) do
    case Bodyguard.permit(Threads, :react_to_thread, user, thread) do
      :ok ->
        %Reaction{}
        |> Reaction.new_changeset(user, thread, attrs)
        |> Repo.insert()

      err ->
        err
    end
  end

  def remove_reaction_from_thread(reaction = %Reaction{}, user = %User{}) do
    case Bodyguard.permit(Threads, :remove_reaction, user, reaction) do
      :ok ->
        reaction
        |> Repo.delete()

      {:error, reason} ->
        {:error, reason}
    end
  end

  def archive_thread(thread = %Thread{}, user = %User{}) do
    case Bodyguard.permit(Threads, :archive_thread, user, thread) do
      :ok ->
        thread
        |> Thread.edit_changeset(%{archived_at: DateTime.utc_now()})
        |> Repo.update()

      err ->
        err
    end
  end

  defp get_thread_with_tags(query, [tag | tail]) do
    query = or_where(query, [t], ^tag in t.tags)
    get_thread_with_tags(query, tail)
  end

  defp get_thread_with_tags(query, _) do
    query
  end
end
