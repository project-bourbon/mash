defmodule Bourbon.Threads.Comment do
  @moduledoc """
  Ecto schema and changesets for thread comments.
  """

  use Ecto.Schema
  import Ecto.Changeset
  alias Bourbon.{Accounts, Threads}
  alias __MODULE__

  schema "comments" do
    belongs_to(:user, Accounts.User)
    belongs_to(:thread, Threads.Thread)
    field(:body, :string)

    timestamps(type: :utc_datetime)
  end

  def changeset(comment = %Comment{}, attrs) do
    comment
    |> cast(attrs, [:user_id, :thread_id, :body])
  end

  def new_changeset(comment = %Comment{}, attrs) do
    comment
    |> changeset(attrs)
  end

  def edit_changeset(comment = %Comment{}, attrs) do
    comment
    |> changeset(attrs)
  end
end
