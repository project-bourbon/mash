defmodule Bourbon.Threads.Thread do
  @moduledoc """
  Ecto schema and changesets for threads.
  """

  use Ecto.Schema
  import Ecto.Changeset
  alias Bourbon.Accounts.User
  alias Bourbon.Threads.Comment
  alias __MODULE__

  schema "threads" do
    belongs_to(:user, User)
    field(:title, :string)
    field(:link, :string)
    field(:text, :string)
    field(:privacy, :string)
    field(:tags, {:array, :string})
    field(:archived_at, :utc_datetime)
    has_many(:comments, Comment, on_delete: :delete_all)

    timestamps(type: :utc_datetime)
  end

  def changeset(thread = %Thread{}, attrs \\ %{}) do
    attrs = attrs |> convert_privacy_to_string!

    thread
    |> cast(attrs, [:user_id, :title, :link, :text, :privacy, :tags])
    |> validate_required([:user_id, :title, :privacy, :tags])
    |> validate_required_link_or_text(attrs)
  end

  def new_changeset(thread = %Thread{}, attrs \\ %{}) do
    thread
    |> changeset(attrs)
  end

  def edit_changeset(thread = %Thread{}, attrs) do
    thread
    |> cast(attrs, [:user_id, :title, :link, :text, :privacy, :tags, :archived_at])
  end

  defp convert_privacy_to_string!(attrs) do
    Map.replace!(attrs, :privacy, Atom.to_string(attrs.privacy))
  end

  defp validate_required_link_or_text(changeset, %{link: _}) do
    changeset
    |> validate_required(:link)
  end

  defp validate_required_link_or_text(changeset, %{text: _}) do
    changeset
    |> validate_required(:text)
  end

  defp validate_required_link_or_text(changeset, _) do
    changeset
    |> add_error(:params, "either :link or :text is required")
  end
end
