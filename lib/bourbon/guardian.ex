defmodule Bourbon.Guardian do
  @moduledoc """
  Guardian module for the API.
  """

  use Guardian, otp_app: :bourbon
  alias Bourbon.Accounts
  alias Bourbon.Accounts.User

  def subject_for_token(%User{} = resource, _claims) do
    {:ok, to_string(resource.id)}
  end

  def subject_for_token(_, _) do
    {:error, :reason_for_error}
  end

  def resource_from_claims(%{"sub" => sub}) do
    {:ok, Accounts.get_user_by_id(sub)}
  end

  def resource_from_claims(_claims) do
    {:error, :reason_for_error}
  end
end
