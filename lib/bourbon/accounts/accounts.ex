defmodule Bourbon.Accounts do
  @moduledoc """
  Functions for interfacing with accounts.
  """

  import Ecto.Query
  alias Bourbon.Repo
  alias Bourbon.Accounts.{User, Credential, Profile}
  alias Ecto.Multi

  def data() do
    Dataloader.Ecto.new(Repo, query: &query/2)
  end

  def query(queryable, _params), do: queryable

  defp add_user(options = %{}) do
    new_profile_attrs = Map.get(options, :profile) || %{}
    options = Map.put(options, :profile, new_profile_attrs)

    %{user: user_attrs, credential: credential_attrs, profile: profile_attrs} = options

    case Multi.new()
         |> Multi.insert(:user, User.new_changeset(%User{}, user_attrs))
         |> Multi.merge(fn %{user: user} ->
           add_user_externals(user, %{credential: credential_attrs, profile: profile_attrs})
         end)
         |> Repo.transaction() do
      {:ok, %{user: user}} -> {:ok, user}
      error -> error
    end
  end

  def create_admin_user(options = %{}) do
    %{user: user_attrs} = options
    new_user_attrs = Map.put(user_attrs, :role, "admin")

    Map.merge(options, %{user: new_user_attrs})
    |> add_user
  end

  def create_user(options = %{}) do
    %{user: user_attrs} = options
    new_user_attrs = Map.drop(user_attrs, [:role])

    Map.merge(options, %{user: new_user_attrs})
    |> add_user
  end

  def get_all_users() do
    Repo.all(User)
  end

  def edit_user(user = %User{}, new_attrs) do
    user
    |> User.edit_changeset(new_attrs)
    |> Repo.update()
  end

  def edit_user_profile(user = %User{}, new_attrs) do
    Profile.edit_changeset(user.profile, new_attrs)
    |> Repo.update()
  end

  def edit_credential(credential = %Credential{}, new_attrs) do
    Credential.edit_changeset(credential, new_attrs)
    |> Repo.update()
  end

  def get_user_by_id(user_id) do
    case Repo.get(User, user_id) do
      user = %User{} -> {:ok, user}
      nil -> {:error, "Could not find user with that ID"}
    end
  end

  def delete_user(user) do
    Repo.delete(user)
  end

  def is_username_available?(username) do
    query = from(u in User, where: u.username == ^username, select: u)

    case Repo.aggregate(query, :count, :id) do
      0 -> true
      _ -> false
    end
  end

  def login_with_email(%{email: email, password: password}) do
    query =
      from(
        c in Credential,
        inner_join: u in assoc(c, :user),
        where: c.type == "email" and c.username == ^email,
        preload: [:user],
        select: c
      )

    case Repo.one(query) do
      credential = %Credential{} ->
        case check_password(credential, password) do
          true -> {:ok, credential.user}
          false -> {:error, :not_found}
        end

      _ ->
        {:error, :not_found}
    end
  end

  defp check_password(credential, password) do
    Comeonin.Argon2.checkpw(password, credential.password_hash)
  end

  defp add_user_externals(user = %User{}, %{credential: credential_attrs, profile: profile_attrs}) do
    credential_attrs = credential_attrs |> Map.put(:user_id, user.id)
    profile_attrs = profile_attrs |> Map.put(:user_id, user.id)

    credential = Credential.new_changeset(%Credential{}, credential_attrs)
    profile = Profile.new_changeset(%Profile{}, profile_attrs)

    Multi.new()
    |> Multi.insert(:credential, credential)
    |> Multi.insert(:profile, profile)
  end
end
