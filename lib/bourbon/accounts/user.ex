defmodule Bourbon.Accounts.User do
  @moduledoc """
  Ecto schema and changesets for users.
  """

  use Ecto.Schema
  import Ecto.{Changeset}
  alias Bourbon.Accounts.{Credential, Profile}
  alias Bourbon.Threads.{Thread, Comment}

  schema "users" do
    field(:username, :string)
    field(:role, :string, default: "user")
    has_many(:credentials, Credential, on_delete: :delete_all)
    has_many(:threads, Thread, on_delete: :delete_all)
    has_many(:comments, Comment, on_delete: :delete_all)
    has_one(:profile, Profile, on_delete: :delete_all)

    timestamps(type: :utc_datetime)
  end

  def changeset(user = %__MODULE__{}, attrs \\ %{}) do
    user
    |> cast(attrs, [:username, :role])
    |> validate_required([:username, :role])
    |> unique_constraint(:username)
  end

  def new_changeset(user = %__MODULE__{}, attrs \\ %{}) do
    user
    |> changeset(attrs)
  end

  def edit_changeset(user = %__MODULE__{}, new_attrs) do
    user
    |> changeset(new_attrs)
  end
end
