defmodule Bourbon.Accounts.Credential do
  @moduledoc """
  Ecto schema and changesets for account credentials.
  """

  use Ecto.Schema
  import Ecto.Changeset
  alias Bourbon.Accounts.{User}

  schema "credentials" do
    field(:type, :string)
    field(:username, :string)
    field(:password, :string, virtual: true)
    field(:password_hash, :string)
    belongs_to(:user, User)

    timestamps(type: :utc_datetime)
  end

  def changeset(credential = %__MODULE__{}, attrs) do
    attrs =
      cond do
        Map.has_key?(attrs, :type) -> attrs |> convert_type_to_string!
        true -> attrs
      end

    credential
    |> cast(attrs, [:user_id, :type, :username, :password])
    |> unique_constraint(:username, name: :credentials_type_username_index)
  end

  def new_changeset(credential = %__MODULE__{}, attrs) do
    credential
    |> changeset(attrs)
    |> put_password_hash()
  end

  def edit_changeset(credential = %__MODULE__{}, attrs) do
    credential
    |> changeset(attrs)
    |> put_password_hash()
  end

  defp convert_type_to_string!(attrs) do
    Map.replace!(attrs, :type, Atom.to_string(attrs.type))
  end

  defp put_password_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: password}} ->
        put_change(changeset, :password_hash, Comeonin.Argon2.hashpwsalt(password))
        |> Map.drop([:password])

      _ ->
        changeset
    end
  end
end
