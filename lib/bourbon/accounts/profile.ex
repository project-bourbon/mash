defmodule Bourbon.Accounts.Profile do
  @moduledoc """
  Ecto schema and changesets for account profiles.
  """

  use Ecto.Schema
  import Ecto.Changeset
  alias Bourbon.Accounts.User

  schema "profiles" do
    field(:name, :string)
    field(:location, :string)
    field(:bio, :string)
    field(:birthdate, :utc_datetime)
    belongs_to(:user, User)

    timestamps(type: :utc_datetime)
  end

  def changeset(profile = %__MODULE__{}, attrs) do
    profile
    |> cast(attrs, [:user_id, :name, :location, :bio, :birthdate])
  end

  def new_changeset(profile = %__MODULE__{}, attrs) do
    profile
    |> changeset(attrs)
  end

  def edit_changeset(profile = %__MODULE__{}, attrs) do
    profile
    |> changeset(attrs)
  end
end
